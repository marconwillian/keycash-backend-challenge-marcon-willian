# Desafio de Marcon Willian Oliveira Neves

## Iniciando Projeto
O projeto pode ser iniciado em modo de desenvolvimento:

Primeiro deve se ter instalado o Docker e Docker Compose para iniciar o DB, caso não tenha o MySql instalado na maquina diretamente, caso escolha usar o Docker, executar os comando na rais do projeto.

```bash
docker-compose up -d

yarn typeorm migration:run
```


Neste caso o projeto em si vai rodar direto da sua maquina com os comandos:

```bash
yarn install

yarn dev
```

Lembrando que neste caso já foi definido no arquivo ormconfig.yml os dados do servidor MySQL criado pelo docker.

Caso prefira configurar seu proprio DB, pode adicionar suas configs no arquivo ormconfig.yml para o CLI ou criar um arquivo .env apartir do modelo .model.env com as configs do servidor, que vai rodar na aplicação.

Após a aplicação rodando voce podera encontrar as docs no path /api/docs

## Debugger With Insomnia
Caso você queira abrir a documentação no insomnia, voce pode clonar este projeto direto com a opção `Create -> Git Clone`. Assim ele já vai importar a documentação Swagger e as Rotas Disponiveis da aplicação.

## Aplicação Online
A aplicação esta rodando no GCP Cloud Run:
- https://keycash.marcon.run/api/docs

A aplicação teve seu deploy e tests de qualidade de código e Lint executados pelos processo de CI/CD do GitLab:
- https://gitlab.com/mh4sh/test/keycash/plano-backend/-/pipelines
