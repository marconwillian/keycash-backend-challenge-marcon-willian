import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class alterPropertyTable1633960878626 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'property',
      new TableColumn({
        name: 'infoBedrooms',
        type: 'int'
      })
    );
    await queryRunner.addColumn(
      'property',
      new TableColumn({
        name: 'infoParkingSpace',
        type: 'int'
      })
    );
    await queryRunner.addColumn(
      'property',
      new TableColumn({
        name: 'infoSize',
        type: 'int'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('property', 'infoBedrooms');
    await queryRunner.dropColumn('property', 'infoParkingSpace');
    await queryRunner.dropColumn('property', 'infoSize');
  }
}
