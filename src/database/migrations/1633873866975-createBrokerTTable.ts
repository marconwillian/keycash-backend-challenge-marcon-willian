import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class createBrokerTTable1633873866975 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'broker',
        columns: [
          {
            name: 'id',
            type: 'int',
            isPrimary: true
          },
          {
            name: 'name',
            type: 'varchar',
            width: 64
          },
          {
            name: 'email',
            type: 'varchar',
            width: 256,
            isUnique: true
          },
          {
            name: 'phoneNumber',
            type: 'varchar',
            width: 16
          },
          {
            name: 'createdAt',
            type: 'timestamp',
            default: 'CURRENT_TIMESTAMP'
          },
          {
            name: 'updatedAt',
            type: 'timestamp',
            default: 'CURRENT_TIMESTAMP',
            onUpdate: 'CURRENT_TIMESTAMP'
          }
        ]
      }),
      true
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('broker');
  }
}
