import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class createPropertyDataTable1633921537723
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'propertyData',
        columns: [
          {
            name: 'id',
            type: 'int',
            isPrimary: true
          },
          {
            name: 'key',
            type: 'varchar',
            width: 16
          },
          {
            name: 'value',
            type: 'varchar'
          },
          {
            name: 'propertyId',
            type: 'int'
          },
          {
            name: 'createdAt',
            type: 'timestamp',
            default: 'CURRENT_TIMESTAMP'
          },
          {
            name: 'updatedAt',
            type: 'timestamp',
            default: 'CURRENT_TIMESTAMP',
            onUpdate: 'CURRENT_TIMESTAMP'
          }
        ]
      }),
      true
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('propertyData');
    await queryRunner.dropTable('propertyToPropertyData');
  }
}
