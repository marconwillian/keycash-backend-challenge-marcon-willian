import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class createPropertyContentTable1633914848209
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'propertyContent',
        columns: [
          {
            name: 'id',
            type: 'int',
            isPrimary: true
          },
          {
            name: 'title',
            type: 'varchar',
            width: 64
          },
          {
            name: 'description',
            type: 'varchar'
          },
          {
            name: 'url',
            type: 'varchar',
            width: 256,
            comment:
              'URL of content, in case of image, the image url, in case of embed, a link of embed'
          },
          {
            name: 'type',
            type: 'enum',
            enum: ['image', 'embed']
          },
          {
            name: 'propertyId',
            type: 'int'
          },
          {
            name: 'createdAt',
            type: 'timestamp',
            default: 'CURRENT_TIMESTAMP'
          },
          {
            name: 'updatedAt',
            type: 'timestamp',
            default: 'CURRENT_TIMESTAMP',
            onUpdate: 'CURRENT_TIMESTAMP'
          }
        ]
      }),
      true
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('propertyContent');
  }
}
