import {
  MigrationInterface,
  QueryRunner,
  TableForeignKey,
  TableIndex
} from 'typeorm';

export class alterAddRelations1633924484121 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createIndex(
      'broker',
      new TableIndex({
        name: 'IDX_BROKER_EMAIL',
        columnNames: ['email'],
        isUnique: true
      })
    );

    await queryRunner.createIndex(
      'propertyData',
      new TableIndex({
        name: 'IDX_PROPERTY_DATA_KEY',
        columnNames: ['key'],
        isUnique: false
      })
    );

    await queryRunner.createForeignKey(
      'property',
      new TableForeignKey({
        columnNames: ['brokerId'],
        referencedColumnNames: ['id'],
        referencedTableName: 'broker',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      })
    );

    await queryRunner.createForeignKey(
      'propertyContent',
      new TableForeignKey({
        columnNames: ['propertyId'],
        referencedColumnNames: ['id'],
        referencedTableName: 'property',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      })
    );

    await queryRunner.createForeignKey(
      'propertyData',
      new TableForeignKey({
        columnNames: ['propertyId'],
        referencedColumnNames: ['id'],
        referencedTableName: 'property',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const tableProperty = await queryRunner.getTable('property');
    const foreignKeyBrokerId = tableProperty.foreignKeys.find(
      fk => fk.columnNames.indexOf('brokerId') !== -1
    );
    await queryRunner.dropForeignKey('property', foreignKeyBrokerId);

    const tablePropertyContent = await queryRunner.getTable('propertyContent');
    const foreignKeyPropertyId = tablePropertyContent.foreignKeys.find(
      fk => fk.columnNames.indexOf('propertyId') !== -1
    );
    await queryRunner.dropForeignKey('propertyContent', foreignKeyPropertyId);

    const tablePropertyData = await queryRunner.getTable('propertyData');
    const foreignKeyPropertyIdData = tablePropertyData.foreignKeys.find(
      fk => fk.columnNames.indexOf('propertyId') !== -1
    );
    await queryRunner.dropForeignKey('propertyData', foreignKeyPropertyIdData);

    await queryRunner.dropIndex('propertyData', 'IDX_PROPERTY_DATA_KEY');
  }
}
