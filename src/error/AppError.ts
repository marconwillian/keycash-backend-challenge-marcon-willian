/* eslint-disable @typescript-eslint/no-unused-vars */
import { NextFunction, Request, Response } from 'express';
import {
  selectStatus,
  Status4XXNames,
  Status5XXNames
} from 'http-status-schema';

type ISingleError = {
  message: string;
  key: string;
};

class AppError {
  public errors: ISingleError[] = [];

  public readonly statusCode: number;

  constructor(
    error: ISingleError[],
    statusCode: number | Status4XXNames | Status5XXNames = 400
  ) {
    this.statusCode =
      typeof statusCode === 'number' ? statusCode : selectStatus(statusCode);

    this.errors = [...this.errors, ...error];
  }

  public static notFountRoute() {
    return (
      request: Request,
      response: Response
    ): void | Response<any, Record<string, any>> => {
      return response.status(404).json({
        errors: [
          {
            code: 'invalid.route',
            message: "Sorry can't find that route!"
          }
        ]
      });
    };
  }

  public static middleware() {
    // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
    return (
      error: Error | AppError,
      request: Request,
      response: Response,
      nextFunction: NextFunction
    ) => {
      console.error(error);

      if (error instanceof AppError) {
        return response.status(error.statusCode).json({
          errors: error.errors
        });
      }

      return response.status(500).json({
        errors: [
          {
            key: 'internal.generic',
            message: 'This application have a internal error.'
          }
        ]
      });
    };
  }
}

export default AppError;
