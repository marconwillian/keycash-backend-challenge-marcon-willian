import App from './app';
import 'dotenv/config';

class Server {
  private static port: number = Number(process.env.PORT) || 8080;

  public static start() {
    App.listen(this.port, () => {
      console.log(`🤘🏾 Project listen on port: ${this.port}`);
    });
  }
}

Server.start();
