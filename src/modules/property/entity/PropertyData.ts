import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';

import { Property } from './Property';

@Entity('propertyData')
export class PropertyData {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ width: 16 })
  key: string;

  @Column()
  value: string;

  @ManyToOne(() => Property, property => property.content, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
  })
  property: Property;

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)'
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)'
  })
  updatedAt: Date;
}
