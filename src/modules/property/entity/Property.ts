import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToMany
} from 'typeorm';

import { Broker } from '../../broker/entity/Broker';
import { PropertyContent } from './PropertyContent';
import { PropertyData } from './PropertyData';

@Entity('property')
export class Property {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ width: 64 })
  title: string;

  @Column()
  description: string;

  @Column()
  amount: number;

  @ManyToOne(() => Broker, broker => broker.properties, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
  })
  broker: number;

  @OneToMany(() => PropertyContent, content => content.property)
  content: PropertyContent[];

  @OneToMany(() => PropertyData, propertyData => propertyData.property)
  data: PropertyData[];

  @Column({ width: 16 })
  latitude: string;

  @Column({ width: 16 })
  longitude: string;

  @Column({ width: 128 })
  addressPublicPlace: string;

  @Column({ width: 16 })
  addressNumber: string;

  @Column({ width: 64 })
  addressNeighborhood: string;

  @Column({ width: 32 })
  addressCity: string;

  @Column({ width: 32 })
  addressState: string;

  @Column()
  infoBedrooms: number;

  @Column()
  infoParkingSpace: number;

  @Column()
  infoSize: number;

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)'
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)'
  })
  updatedAt: Date;
}
