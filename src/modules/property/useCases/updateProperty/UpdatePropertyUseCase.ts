import { inject, injectable } from 'tsyringe';

import AppError from '../../../../error/AppError';
import { IBrokerRepository } from '../../../broker/repositories/IBrokerRepository';
import { IPropertyRepository } from '../../repositories/IPropertyRepository';

interface IRequestInfos {
  brokerEmail: string;
  propertyId: number;
}

interface IRequest {
  title?: string;
  description?: string;

  location?: {
    latitude?: string;
    longitude?: string;
  };

  address?: {
    publicPlace?: string;
    number?: string;
    neighborhood?: string;
    city?: string;
    state?: string;
  };

  broker?: {
    name?: string;
    email?: string;
    phoneNumber?: string;
  };

  info?: {
    amount?: number;
    bedrooms?: number;
    parkingSpace?: number;
    size?: number;
  };
}

@injectable()
class UpdatePropertyUseCase {
  constructor(
    @inject('BrokerRepository')
    private brokerRepository: IBrokerRepository,
    @inject('PropertyRepository')
    private propertyRepository: IPropertyRepository
  ) {}

  async execute(
    { brokerEmail, propertyId }: IRequestInfos,
    { title, description, location, broker, address, info }: IRequest
  ): Promise<void> {
    const brokerAlreadyExist = await this.brokerRepository.findByEmail(
      brokerEmail
    );

    if (!brokerAlreadyExist) {
      throw new AppError(
        [
          {
            key: 'property.update.brokerNotExist',
            message: 'This Broker email not exist.'
          }
        ],
        '400 Bad Request'
      );
    }

    await this.brokerRepository.updateByThis(brokerAlreadyExist, broker);

    const { affected } = await this.propertyRepository.updateById(
      propertyId,
      {
        title,
        description,
        amount: info && info.amount ? info.amount : undefined,
        location,
        address,
        ...(info && {
          info: {
            bedrooms: info.bedrooms || undefined,
            parkingSpace: info.parkingSpace || undefined,
            size: info.size || undefined
          }
        })
      },
      brokerAlreadyExist.id
    );

    if (affected === 0) {
      throw new AppError(
        [
          {
            key: 'property.update.propertyNotExist',
            message: 'This property had not found.'
          }
        ],
        '404 Not Found'
      );
    }
  }
}

export { UpdatePropertyUseCase };
