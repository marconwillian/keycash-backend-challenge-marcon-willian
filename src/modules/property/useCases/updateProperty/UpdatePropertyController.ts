import { celebrate, Joi, Segments } from 'celebrate';
import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { UpdatePropertyUseCase } from './UpdatePropertyUseCase';

class UpdatePropertyController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { title, description, location, address, broker, info } =
      request.body;
    const { propertyId } = request.params;
    const { 'x-broker-email': brokerEmail } = request.headers;

    const updatePropertyUseCase = container.resolve(UpdatePropertyUseCase);

    await updatePropertyUseCase.execute(
      {
        propertyId: Number(propertyId),
        brokerEmail: String(brokerEmail)
      },
      {
        title,
        description,
        location,
        address,
        broker,
        info
      }
    );

    return response.status(204).json({});
  }

  validate = celebrate(
    {
      [Segments.BODY]: Joi.object().keys({
        title: Joi.string().max(64),
        description: Joi.string(),
        location: Joi.object().keys({
          latitude: Joi.string().max(16),
          longitude: Joi.string().max(16)
        }),
        address: Joi.object().keys({
          publicPlace: Joi.string().max(128),
          number: Joi.string().max(16),
          neighborhood: Joi.string().max(64),
          city: Joi.string().max(32),
          state: Joi.string().max(32)
        }),
        broker: Joi.object().keys({
          name: Joi.string().max(64),
          email: Joi.string().email(),
          phoneNumber: Joi.string().max(16)
        }),
        info: Joi.object().keys({
          amount: Joi.number(),
          bedrooms: Joi.number(),
          parkingSpace: Joi.number(),
          size: Joi.number()
        })
      })
    },
    {
      abortEarly: false
    }
  );
}

export { UpdatePropertyController };
