import { celebrate, Joi, Segments } from 'celebrate';
import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { ListPropertyUseCase } from './ListPropertyUseCase';

class ListPropertyController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { bedrooms, parkingSpace, size } = request.query;
    const listPropertyUseCase = container.resolve(ListPropertyUseCase);

    const properties = await listPropertyUseCase.execute({
      bedrooms: Number(bedrooms),
      parkingSpace: Number(parkingSpace),
      size: Number(size)
    });

    return response.status(200).json({ result: properties });
  }

  validate = celebrate(
    {
      [Segments.QUERY]: Joi.object().keys({
        bedrooms: Joi.number(),
        parkingSpace: Joi.number(),
        size: Joi.number()
      })
    },
    {
      abortEarly: false
    }
  );
}

export { ListPropertyController };
