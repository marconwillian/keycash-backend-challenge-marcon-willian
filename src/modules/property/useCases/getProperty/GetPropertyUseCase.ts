import { inject, injectable } from 'tsyringe';

import AppError from '../../../../error/AppError';
import { Property } from '../../entity/Property';
import { IPropertyRepository } from '../../repositories/IPropertyRepository';

interface IInfo {
  [key: string]: string | number;
}

interface IProperty extends Property {
  info: IInfo;
}

interface IRequest {
  propertyId: number;
}

@injectable()
class GetPropertyUseCase {
  constructor(
    @inject('PropertyRepository')
    private propertyRepository: IPropertyRepository
  ) {}

  async execute({ propertyId }: IRequest): Promise<IProperty> {
    const property = await this.propertyRepository.findById(propertyId, [
      'broker',
      'content',
      'data'
    ]);

    if (!property) {
      throw new AppError(
        [
          {
            key: 'property.get.notFound',
            message: 'Not found this property'
          }
        ],
        '404 Not Found'
      );
    }

    const info = (property.data || []).reduce(
      (prevItem, actualItem) => {
        return {
          ...prevItem,
          [actualItem.key]: Number(actualItem.value) || actualItem.value
        };
      },
      {
        bedrooms: property.infoBedrooms,
        parkingSpace: property.infoParkingSpace,
        size: property.infoSize
      } as IInfo
    );

    delete property.data;
    delete property.infoBedrooms;
    delete property.infoParkingSpace;
    delete property.infoSize;

    return { ...property, info };
  }
}

export { GetPropertyUseCase };
