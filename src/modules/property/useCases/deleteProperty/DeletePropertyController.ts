import { celebrate, Joi, Segments } from 'celebrate';
import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { DeletePropertyUseCase } from './DeletePropertyUseCase';

class DeletePropertyController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { propertyId } = request.params;
    const { 'x-broker-email': brokerEmail } = request.headers;

    const deletePropertyUseCase = container.resolve(DeletePropertyUseCase);

    await deletePropertyUseCase.execute({
      propertyId: Number(propertyId),
      brokerEmail: String(brokerEmail)
    });

    return response.status(200).json({});
  }

  validate = celebrate(
    {
      [Segments.HEADERS]: Joi.object()
        .keys({
          'x-broker-email': Joi.string().email().required()
        })
        .unknown(),
      [Segments.PARAMS]: Joi.object().keys({
        propertyId: Joi.number().required()
      })
    },
    {
      abortEarly: false
    }
  );
}

export { DeletePropertyController };
