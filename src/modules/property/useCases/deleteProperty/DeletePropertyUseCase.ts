import { inject, injectable } from 'tsyringe';

import AppError from '../../../../error/AppError';
import { IBrokerRepository } from '../../../broker/repositories/IBrokerRepository';
import { IPropertyRepository } from '../../repositories/IPropertyRepository';

interface IRequest {
  propertyId: number;
  brokerEmail: string;
}

@injectable()
class DeletePropertyUseCase {
  constructor(
    @inject('BrokerRepository')
    private brokerRepository: IBrokerRepository,
    @inject('PropertyRepository')
    private propertyRepository: IPropertyRepository
  ) {}

  async execute({ propertyId, brokerEmail }: IRequest): Promise<void> {
    const broker = await this.brokerRepository.findByEmail(brokerEmail);

    if (!broker) {
      throw new AppError(
        [
          {
            key: 'property.delete.brokerNotExist',
            message: 'This Broker email not exist.'
          }
        ],
        '400 Bad Request'
      );
    }

    const { affected } = await this.propertyRepository.deleteById(
      propertyId,
      broker.id
    );

    if (affected === 0) {
      throw new AppError(
        [
          {
            key: 'property.delete.propertyNotExist',
            message: 'This property had not found.'
          }
        ],
        '404 Not Found'
      );
    }
  }
}

export { DeletePropertyUseCase };
