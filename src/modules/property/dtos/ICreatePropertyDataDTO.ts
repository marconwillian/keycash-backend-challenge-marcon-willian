interface ICreatePropertyDataDTO {
  key: string;
  value: string | number;
}

export { ICreatePropertyDataDTO };
