interface IInfoFilterDTO {
  bedrooms?: number;
  parkingSpace?: number;
  size?: number;
}

export { IInfoFilterDTO };
