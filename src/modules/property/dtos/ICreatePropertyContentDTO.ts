interface ICreatePropertyContentDTO {
  title: string;
  description: string;
  url: string;
  type: 'image' | 'embed';
}

export { ICreatePropertyContentDTO };
