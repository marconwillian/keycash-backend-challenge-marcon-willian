import { getRepository, Repository } from 'typeorm';

import { ICreatePropertyContentDTO } from '../../dtos/ICreatePropertyContentDTO';
import { PropertyContent } from '../../entity/PropertyContent';
import { IPropertyContentRepository } from '../IPropertyContentRepository';

class PropertyContentRepository implements IPropertyContentRepository {
  private repository: Repository<PropertyContent>;

  constructor() {
    this.repository = getRepository(PropertyContent);
  }
  async createMany(
    data: ICreatePropertyContentDTO[]
  ): Promise<PropertyContent[]> {
    const contents = this.repository.create(data);

    return this.repository.save(contents);
  }
}

export { PropertyContentRepository };
