import { ICreatePropertyDataDTO } from '../dtos/ICreatePropertyDataDTO';
import { PropertyData } from '../entity/PropertyData';

interface IPropertyDataRepository {
  createMany(data: ICreatePropertyDataDTO[]): Promise<PropertyData[]>;
}

export { IPropertyDataRepository };
