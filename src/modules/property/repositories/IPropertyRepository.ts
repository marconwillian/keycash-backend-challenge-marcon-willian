import { ICreatePropertyDTO } from '../dtos/ICreatePropertyDTO';
import { IInfoFilterDTO } from '../dtos/IInfoFilterDTO';
import { IUpdatePropertyDTO } from '../dtos/IUpdatePropertyDTO';
import { Property } from '../entity/Property';

export type PropertyExtraInformation = 'broker' | 'content' | 'data';

interface IPropertyRepository {
  create(data: ICreatePropertyDTO): Promise<number>;
  findById(
    propertyId: number,
    include?: PropertyExtraInformation[]
  ): Promise<Property>;
  list(
    include?: PropertyExtraInformation[],
    infoFilter?: IInfoFilterDTO
  ): Promise<Property[]>;
  updateById(
    propertyId: number,
    data: IUpdatePropertyDTO,
    brokerId?: number
  ): Promise<{ affected: number }>;
  deleteById(
    propertyId: number,
    brokerId?: number
  ): Promise<{ affected: number }>;
}

export { IPropertyRepository };
