interface ICreateBrokerDTO {
  name: string;
  email: string;
  phoneNumber: string;
}

export { ICreateBrokerDTO };
