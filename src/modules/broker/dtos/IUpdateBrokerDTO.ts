interface IUpdateBrokerDTO {
  name?: string;
  email?: string;
  phoneNumber?: string;
}

export { IUpdateBrokerDTO };
