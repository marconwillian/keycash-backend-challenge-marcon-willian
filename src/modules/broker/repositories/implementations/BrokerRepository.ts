import { getRepository, Repository } from 'typeorm';

import { ICreateBrokerDTO } from '../../dtos/ICreateBrokerDTO';
import { IUpdateBrokerDTO } from '../../dtos/IUpdateBrokerDTO';
import { Broker } from '../../entity/Broker';
import { IBrokerRepository } from '../IBrokerRepository';

class BrokerRepository implements IBrokerRepository {
  private repository: Repository<Broker>;

  constructor() {
    this.repository = getRepository(Broker);
  }
  async updateByThis(broker: Broker, data: IUpdateBrokerDTO): Promise<Broker> {
    Object.assign(broker, data);

    await this.repository.save(broker);

    return broker;
  }

  async create({
    name,
    email,
    phoneNumber
  }: ICreateBrokerDTO): Promise<number> {
    const broker = this.repository.create({
      name,
      email,
      phoneNumber
    });

    await this.repository.save(broker);

    return broker.id;
  }

  async findByEmail(email: string): Promise<Broker> {
    const broker = await this.repository.findOne({ email });

    return broker;
  }
}

export { BrokerRepository };
