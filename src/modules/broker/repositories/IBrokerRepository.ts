import { ICreateBrokerDTO } from '../dtos/ICreateBrokerDTO';
import { IUpdateBrokerDTO } from '../dtos/IUpdateBrokerDTO';
import { Broker } from '../entity/Broker';

interface IBrokerRepository {
  create(data: ICreateBrokerDTO): Promise<number>;
  findByEmail(email: string): Promise<Broker>;
  updateByThis(broker: Broker, data: IUpdateBrokerDTO): Promise<Broker>;
}

export { IBrokerRepository };
