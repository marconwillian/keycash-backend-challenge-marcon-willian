import { errors as middlewareValidateErrorsCelebrate } from 'celebrate';
import Express from 'express';
import 'express-async-errors';
import 'reflect-metadata';
import { createConnection, getConnectionOptions } from 'typeorm';

import './shared/container';

import AppError from './error/AppError';
import Routes from './routes';

interface IOptions {
  host: string;
  database: string;
  username: string;
  password: string;
}

class App {
  public express: Express.Application;

  constructor() {
    this.express = Express();

    this.database().then(() => {
      this.middlewares();
      this.routes();

      this.validateErros();
    });
  }

  private async database() {
    const options = await getConnectionOptions();

    const newOptions = options as IOptions;

    newOptions.host = process.env.DATABASE_HOST;
    newOptions.database = process.env.DATABASE;
    newOptions.username = process.env.DATABASE_USER;
    newOptions.password = process.env.DATABASE_PASS;

    await createConnection({
      ...options
    });
  }

  private middlewares(): void {
    this.express.use(Express.json());
  }

  private routes(): void {
    this.express.use(Routes);
  }

  private validateErros(): void {
    this.express.use(AppError.notFountRoute());

    this.express.use(middlewareValidateErrorsCelebrate());

    this.express.use(AppError.middleware());
  }
}

export default new App().express;
